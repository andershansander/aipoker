#ifndef CARDDECK_H
#define CARDDECK_H

#include "Card.h"

#include <vector>

using std::vector;

class CardDeck
{
    public:
        CardDeck();
        virtual ~CardDeck();
        void shuffle();
        Card* getTopCard();
        void insertCard(Card* card);
        int cardsInDeck();
    protected:
    private:
        vector<Card*> cards;
};

#endif // CARDDECK_H
