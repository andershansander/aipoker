#ifndef PLAYER_H
#define PLAYER_H

#include "Card.h"
#include "PokerAI.h"

class Player
{
    public:
        Player(bool AIControlled, PokerAI* pokerAI);
        virtual ~Player();

        Card* getCard1();
        Card* getCard2();

        void giveNewHand(Card* card1, Card* card2);
        void addInvestedInHand(double amount);
        double getInvestedInHand();
        double getMoney();
        void addMoney(double amount);

        bool hasFolded();
        bool isAllIn();

        PokerAI* getAI();

        void fold();
        bool isAIControlled();
    protected:
    private:
        Card* card1;
        Card* card2;
        double money;
        double investedInHand;
        bool AIControlled;
        bool folded;
        PokerAI* pokerAI;
};

#endif // PLAYER_H
