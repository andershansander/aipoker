#ifndef HANDVALUE_H
#define HANDVALUE_H

#include <vector>

using std::vector;

class HandValue
{
    public:
        enum HandType {HIGH_CARD, PAIR, TWO_PAIRS, THREE_OF_A_KIND, STRAIGHT, FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH};

        HandValue(HandType handType);


        /**
        Adds a kicker value. The first added kicker value will be the most significant.
        */
        void addKickerValue(int kickerValue);

        int getKickerValue(int kickerIndex);

        bool betterThan(HandValue otherValue);
        bool worseThan(HandValue otherValue);
        bool equalTo(HandValue otherValue);

        HandType getHandType();

        virtual ~HandValue();
    protected:
    private:
        vector<int> kickerValues;
        HandType handType;
};

#endif // HANDVALUE_H
