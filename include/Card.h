#ifndef CARD_H
#define CARD_H

class Card
{
    public:
        enum Suit {HEARTS, CLUBS, SPADES, DIAMONDS};
        Card(int rank, Suit suit);
        virtual ~Card();
        Suit getSuit();
        int getRank();
    protected:
    private:
        const int rank;
        const Suit suit;
};

#endif // CARD_H
