#ifndef REINFORCEMENTLEARNINGPOKERAI_H
#define REINFORCEMENTLEARNINGPOKERAI_H

#include <string>
#include <map>
#include <vector>

#include "PokerAI.h"
#include "ActionEvaluation.h"

using std::string;
using std::vector;
using std::map;

class ReinforcementLearningPokerAI : public PokerAI
{
    public:
        ReinforcementLearningPokerAI(double learningRate, double discountFactor, double maximumExpectedActionValue);
        virtual ~ReinforcementLearningPokerAI();
        virtual Action* decideNextPreFlopAction(string situationKey, double betSize, bool canCheck, bool canBet);
        virtual Action* decideNextPostFlopAction(string situationKey, double betSize, bool canCheck, bool canBet);


        void writeToFile(string preFlopFilename, string postFlopFilename);
        void loadFromFile(string preFlopFilename, string postFlopFilename);

        /**
        Called when a hand is finished to learn from all actions taken in the hand
        */
        void learnFromLastHand(double reward);
    protected:
    private:
        map<string, ActionEvaluation*> preFlopActionMap;
        map<string, vector<Action::Type>*> preFlopActionHistory;

        map<string, ActionEvaluation*> postFlopActionMap;
        map<string, vector<Action::Type>*> postFlopActionHistory;

        const double learningRate;
        const double discountFactor;
        const double maximumExpectedActionValue;

        void updateActionEvaluationFromFileLine(ActionEvaluation* actionEvaluation, string line);
};

#endif // REINFORCEMENTLEARNINGPOKERAI_H
