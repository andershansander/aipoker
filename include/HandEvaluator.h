#ifndef HANDEVALUATOR_H
#define HANDEVALUATOR_H

#include <vector>

#include"HandValue.h"
#include "Card.h"

using std::vector;

class HandEvaluator
{
    public:
        HandEvaluator();
        virtual ~HandEvaluator();

        HandValue getHandValue(vector<Card*> cards);

        int flushCardsInHand(vector<Card*> cards);
    protected:
    private:
        HandValue get5CardHandValue(vector<Card*> cards);
};

#endif // HANDEVALUATOR_H
