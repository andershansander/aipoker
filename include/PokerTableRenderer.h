#ifndef POKERTABLERENDERER_H
#define POKERTABLERENDERER_H

#include <SFML/Graphics.hpp>

//#include "primitiveGui.h"
#include "PokerTable.h"



class PokerTableRenderer
{
    public:
        PokerTableRenderer(PokerTable* table, sf::RenderWindow* window);
        virtual ~PokerTableRenderer();

        void draw();
        void handleLeftMouseClick(sf::Event &event);
    protected:
    private:
        PokerTable* pokerTable;
        sf::RenderWindow* window;

        sf::Font font;
        sf::Text infoText;
        sf::Color foldedColor;
        sf::Color currentPlayerColor;

        //sprites and textures
        sf::Sprite cardSprites[52];
        sf::Texture cardTexture[52];
        sf::Sprite cardBackside;
        sf::Texture cardBacksideTexture;
        sf::Sprite table;
        sf::Texture tableTexture;
        sf::Sprite bigBlindMarker;
        sf::Texture bigBlindTexture;
        sf::Sprite smallBlindMarker;
        sf::Texture smallBlindTexture;
        sf::Sprite foldButtonSprite;
        sf::Texture foldButtonTexture;
        sf::Sprite callCheckButtonSprite;
        sf::Texture callCheckButtonTexture;
        sf::Sprite betButtonSprite;
        sf::Texture betButtonTexture;
        sf::Sprite nextHandSprite;
        sf::Texture nextHandTexture;


        void loadTextures();
        void drawCard(int x, int y, Card* card, bool folded, bool currentPlayer);
        void drawInputControls();
        void drawBoard();

        bool actionButtonsActivated();
        bool nextHandButtonActivated();

        const static int BOARD_WIDTH = 1200;
        const static int BOARD_HEIGHT = 900;

        const static int BUTTON_WIDTH = 250;
        const static int BUTTON_HEIGHT = 90;
        const static int BUTTON_X = 1225;
        const static int FOLD_BUTTON_Y = 200;
        const static int CHECK_BUTTON_Y = 400;
        const static int BET_BUTTON_Y = 600;
        const static int NEXT_HAND_BUTTON_Y = 800;

        const static float CARD_SCALE = 0.5f;

        const static int UNSCALED_CARD_WIDTH = 300;
        const static int UNSCALED_CARD_HEIGHT = 400;

        int CARD_WIDTH;
        int CARD_HEIGHT;

        int playerLocations[12];
};

#endif // POKERTABLERENDERER_H
