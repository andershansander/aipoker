#ifndef POKERTABLE_H
#define POKERTABLE_H
#include <vector>

#include "Player.h"
#include "CardDeck.h"
#include "Hand.h"
#include "HandEvaluator.h"
#include "SituationGeneralizer.h"

using std::vector;

class PokerTable
{
    public:
        PokerTable(bool needNextHandConfirmation, long maximumNumberOfHandsToPlay);
        virtual ~PokerTable();

        Hand* getCurrentHand();
        bool hasOngoingHand();

        void update();

        void currentPlayerFolded();
        void currentPlayerCalled();
        void currentPlayerBetted();

        void addPlayer(Player* player);
        vector<Player*> getPlayers();
        int getPlayerCount();
        int getSmallBlindLocation();

        void allowToProceedToNextHand();
    protected:
    private:
        vector<Player*> players;
        Hand* currentHand;
        CardDeck* cardDeck;
        int smallBlindLocation;
        HandEvaluator* handEvaluator;

        void dealNewHand();
        void handleHandStateChanged();

        const bool needNextHandConfirmation;
        const long maximumNumberOfHandsToPlay;
        long handsPlayed;
        SituationGeneralizer situationGeneralizer;

        bool currentPlayerAllowedToAct();
        bool proceedToNextHand;
        bool hasSavedAI;

        const static int SMALL_BLIND_SIZE = 10;
};

#endif // POKERTABLE_H
