#ifndef POKERAI_H
#define POKERAI_H

#include "Action.h"

#include <string>

using std::string;

class PokerAI
{
    public:
        PokerAI();
        virtual ~PokerAI();
        virtual Action* decideNextPreFlopAction(string situationKey, double betSize, bool canCheck, bool canBet) = 0;
        virtual Action* decideNextPostFlopAction(string situationKey, double betSize, bool canCheck, bool canBet) = 0;

        virtual void learnFromLastHand(double reward) = 0;
        virtual void writeToFile(string preFlopFilename, string postFlopFilename) = 0;
        virtual void loadFromFile(string preFlopFilename, string postFlopFilename) = 0;
    protected:
    private:
};

#endif // POKERAI_H
