#ifndef ACTION_H
#define ACTION_H


class Action
{
    public:
        //the continue signal is used to proceed with the game when no player is allowed to act
        enum Type {FOLD, CALL, BET, CONTINUE_SIGNAL, STATE_CHANGE};

        Action(Type type, double amount);
        virtual ~Action();

        double getAmount();
        Type getType();
    protected:
    private:
        const double amount;
        const Type type;
};

#endif // ACTION_H
