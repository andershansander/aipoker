#ifndef ACTIONEVALUATION_H
#define ACTIONEVALUATION_H

#include <map>

#include "Action.h"

using std::map;

class ActionEvaluation
{
    public:
        ActionEvaluation(double learningRate, double discountFactor, double probableMaxValue);
        virtual ~ActionEvaluation();

        void updateEvaluation(Action::Type type, double reward);
        void setEvaluation(Action::Type type, double reward);
        Action::Type getNextActionToTry(bool canCheck, bool canBet);

        double getActionValue(Action::Type type);
    protected:
    private:
        const double learningRate;
        const double discountFactor;
        const double probableMaxValue;

        map<Action::Type, double> actionValueMap;
};

#endif // ACTIONEVALUATION_H
