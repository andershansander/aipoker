#ifndef HAND_H
#define HAND_H

#include "Action.h"
#include "HandEvaluator.h"
#include "Player.h"

#include <vector>

using std::vector;

/**
    This class represents a poker hand (a round). A poker hand has the following states NOT_STARTED, PREFLOP,
    FLOP, TURN, RIVER and OVER.

    The state will update automatically when enough actions are added. It has to be started manually after players
    have been added though. Adding action is the method for pushing the hand state forward.

    NOTE:
    Added actions will be deleted when this hand is deleted so they should not be referenced outside of the Hand class.
*/
class Hand
{
    public:
        Hand(int smallBlindLocation, double smallBlindSize, HandEvaluator* handEvaluator);
        virtual ~Hand();

        /**
            Returns the next player to act.

            Will throw an exception if the hand is NOT_STARTED or OVER.
        */
        Player* getNextPlayer();
        double getPotSize();

        /**
            Returns true if the player is or was in this hand when it started.
        */
        bool inHand(Player* player);

        /**
            Adds player to this hand.

            Will throw an exception if called when the hand's state is any other than NOT_STARTED
        */
        void addPlayer(Player* player);

        /**
            Returns true if anyone in the hand is allowed to act. Otherwise a CONTINUE_SIGNAL action need to be performed
        */
        bool anyPlayerMayAct();

        bool isOver();
        bool isStarted();
        bool didLastActionCauseStateChange();
        bool hasEnoughPlayersToStart();
        bool playerMayJoin(Player* player);
        bool isPreFlop();
        bool maximumNumberOfBetsReached();

        int getNumberOfPreflopBets();
        int getNumberOfPostflopBets();
        int getNumberOfPlayersAtFlop();
        int getLivePlayersRemaining();

        int numberCommonCardsNeeded();

        double getLargestBet();

        double getBetSize();

        /**
            Sets the state to PREFLOP.

            Will throw an exception if called when the hand's state is any other than NOT_STARTED
        */
        void startHand();

        /**
        This is the method to push the state of the hand forward. If no player is allowed to act a continue signal action
        can be sent to continue the hand.

        Added actions will be deleted when this hand is deleted so they should not be referenced outside of the Hand class.
        Create anonymous objects "new Action(...)" when calling this method.
        */
        void addAction(Player* player, Action* action);

        const vector<Player*> getPlayers();

        vector<Card*> getCommonCards();
        void dealCommonCard(Card* card);
    protected:
    private:
        enum State {NOT_STARTED, PREFLOP, FLOP, TURN, RIVER, OVER};

        State currentState;

        int numberActionsSinceStateStart;
        int dynamicPlayersAtStartOfState; //players that are not folded or all in
        double largestBet;
        int numberOfBetsInState;

        int numberOfPlayersAtFlop;

        int commonCardsNeeded;
        bool lastActionCausedStateChange;

        int smallBlindLocation;
        int nextPlayerToAct;
        double smallBlindSize;

        vector<Player*> players;
        vector<Player*> winner;
        vector<Action*> actions;

        vector<Card*> commonCards;
        HandEvaluator* handEvaluator;

        bool allHadChanceToAct();
        void nextState();
        void onHandOver();
        void decideNextPlayer();
};

#endif // HAND_H
