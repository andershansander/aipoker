#ifndef POT_H
#define POT_H

#include <vector>

#include "HandEvaluator.h"
#include "Player.h"

class Pot
{
    public:
        Pot(HandEvaluator* handEvaluator);
        virtual ~Pot();

        void decidePot(vector<Card*> commonCards);
        void addPlayer(Player* player);
        void addChips(double amount);
        double getPotSize();
    protected:
    private:
        vector<Player*> players;
        double potSize;
        HandEvaluator* handEvaluator;
};

#endif // POT_H
