#ifndef SITUATIONGENERALIZER_H
#define SITUATIONGENERALIZER_H

#include "Card.h"
#include "HandValue.h"

#include <string>

using std::string;

class SituationGeneralizer
{
    public:
        SituationGeneralizer();
        virtual ~SituationGeneralizer();

        string generatePreFlopSituationKey(int numPlayers, int bets, Card* card1, Card* card2);

        string generatePostFlopSituationKey(int playersWhoSawFlop,
                                            int playersRemaining,
                                            int bets,
                                            int flushCardsOnBoard,
                                            int flushCardsInOwnHand,
                                            HandValue commonCardValue,
                                            HandValue ownHandValue);
    protected:
    private:
};

#endif // SITUATIONGENERALIZER_H
