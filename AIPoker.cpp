#include <SFML/Graphics.hpp>
#include "PokerTable.h"
#include "PokerTableRenderer.h"
#include "ReinforcementLearningPokerAI.h"

void updateGame(sf::Time elapsed, PokerTable* pokerTable) {
    pokerTable->update();
}

void handleInput(sf::Event &event, sf::RenderWindow* window, PokerTableRenderer* pokerTableRenderer) {
    if (event.type == sf::Event::Closed) {
        window->close();
    }

    if (event.type == sf::Event::MouseButtonPressed)
    {
        if (event.mouseButton.button == sf::Mouse::Left)
        {
            pokerTableRenderer->handleLeftMouseClick(event);
        }
    }
}

int main()
{
    sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(1500, 900), "AI Poker!");

    PokerTable* pokerTable = new PokerTable(true, 1000000);
    PokerTableRenderer* pokerTableRenderer = new PokerTableRenderer(pokerTable, window);

    pokerTable->addPlayer(new Player(false, 0));

    ReinforcementLearningPokerAI* ai = new ReinforcementLearningPokerAI(0.05, 0.0, 30);
    //ai->loadFromFile("preFlopAI0.txt", "postFlopAI0.txt");
    //pokerTable->addPlayer(new Player(true, ai));

    ai = new ReinforcementLearningPokerAI(0.10, 0.0, 30);
    ai->loadFromFile("preFlopAI1.txt", "postFlopAI1.txt");
    pokerTable->addPlayer(new Player(true, ai));

    ai = new ReinforcementLearningPokerAI(0.15, 0.0, 30);
    ai->loadFromFile("preFlopAI2.txt", "postFlopAI2.txt");
    pokerTable->addPlayer(new Player(true, ai));

    ai = new ReinforcementLearningPokerAI(0.20, 0.0, 30);
    ai->loadFromFile("preFlopAI3.txt", "postFlopAI3.txt");
    pokerTable->addPlayer(new Player(true, ai));

    ai = new ReinforcementLearningPokerAI(0.25, 0.0, 30);
    ai->loadFromFile("preFlopAI4.txt", "postFlopAI4.txt");
    pokerTable->addPlayer(new Player(true, ai));

    ai = new ReinforcementLearningPokerAI(0.30, 0.0, 30);
    ai->loadFromFile("preFlopAI5.txt", "postFlopAI5.txt");
    pokerTable->addPlayer(new Player(true, ai));

    for(int i = 0; i < pokerTable->getPlayerCount(); i++) {
        pokerTable->getPlayers().at(i)->addMoney(2000);
    }

    sf::Clock clock;
    while (window->isOpen())
    //while (true)
    {
        sf::Event event;
        while (window->pollEvent(event))
        {
            handleInput(event, window, pokerTableRenderer);
        }

        sf::Time elapsed = clock.restart();
        updateGame(elapsed, pokerTable);

        pokerTableRenderer->draw();
    }

    return 0;
}

