
#include "PokerTableRenderer.h"

#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>
#include <vector>

#include "Card.h"

using std::wostringstream;
using std::ostringstream;
using std::cout;
using std::vector;

void PokerTableRenderer::loadTextures() {
    char colors[4] = {'k', 's', 'r', 'h'};
    for(int colorIndex = 0; colorIndex < 4; colorIndex++) {
        for(int rankIndex = 2; rankIndex <= 14; rankIndex++) {
            const int arrayIndex = colorIndex * 13 + rankIndex - 2;
            ostringstream os;
            os << "assets/" << colors[colorIndex] << rankIndex << ".png";

            if (!cardTexture[arrayIndex].loadFromFile(os.str()))
            {
                cout << "Couldn't load card image " << os.str() << "\n";
            } else {
                cardSprites[arrayIndex].setTexture(cardTexture[arrayIndex]);
                cardSprites[arrayIndex].setScale(sf::Vector2f(CARD_SCALE, CARD_SCALE));
            }
        }
    }
    cardBacksideTexture.loadFromFile("assets/backside.png");
    cardBackside.setTexture(cardBacksideTexture);
    cardBackside.setScale(sf::Vector2f(CARD_SCALE, CARD_SCALE));

    tableTexture.loadFromFile("assets/table.png");
    table.setTexture(tableTexture);

    smallBlindTexture.loadFromFile("assets/smallBlindMarker.png");
    smallBlindMarker.setTexture(smallBlindTexture);
    bigBlindTexture.loadFromFile("assets/bigBlindMarker.png");
    bigBlindMarker.setTexture(bigBlindTexture);

    foldButtonTexture.loadFromFile("assets/foldButton.png");
    foldButtonSprite.setTexture(foldButtonTexture);
    callCheckButtonTexture.loadFromFile("assets/checkButton.png");
    callCheckButtonSprite.setTexture(callCheckButtonTexture);
    betButtonTexture.loadFromFile("assets/betButton.png");
    betButtonSprite.setTexture(betButtonTexture);

    nextHandTexture.loadFromFile("assets/nextHand.png");
    nextHandSprite.setTexture(nextHandTexture);

}



PokerTableRenderer::PokerTableRenderer(PokerTable* pokerTable, sf::RenderWindow* window) : pokerTable(pokerTable), window(window)
{
    loadTextures();
    foldedColor = sf::Color(150, 50, 50);
    currentPlayerColor = sf::Color(50, 150, 50);

    foldButtonSprite.setPosition(BUTTON_X, FOLD_BUTTON_Y);
    callCheckButtonSprite.setPosition(BUTTON_X, CHECK_BUTTON_Y);
    betButtonSprite.setPosition(BUTTON_X, BET_BUTTON_Y);
    nextHandSprite.setPosition(BUTTON_X, NEXT_HAND_BUTTON_Y);

    CARD_WIDTH = UNSCALED_CARD_WIDTH * CARD_SCALE;
    CARD_HEIGHT = UNSCALED_CARD_HEIGHT * CARD_SCALE;

    const int LEFT = 1;
    const int MIDDLE_X = (BOARD_WIDTH - CARD_WIDTH * 2) / 2;
    const int RIGHT = BOARD_WIDTH - CARD_WIDTH * 2;

    const int TOP = 31;
    const int BOTTOM = BOARD_HEIGHT - CARD_HEIGHT;

    playerLocations[0] = LEFT;
    playerLocations[1] = TOP;

    playerLocations[2] = MIDDLE_X;
    playerLocations[3] = TOP;

    playerLocations[4] = RIGHT;
    playerLocations[5] = TOP;

    playerLocations[6] = RIGHT;
    playerLocations[7] = BOTTOM;

    playerLocations[8] = MIDDLE_X;
    playerLocations[9] = BOTTOM;

    playerLocations[10] = LEFT;
    playerLocations[11] = BOTTOM;

    font.loadFromFile("assets/TTWPGOTT.ttf");
    infoText.setFont(font);
    infoText.setCharacterSize(28);
    infoText.setColor(sf::Color::Black);
}

PokerTableRenderer::~PokerTableRenderer()
{
    //dtor
}

void PokerTableRenderer::drawBoard() {
    window->draw(table);

    //draw common cards and pot size
    if(pokerTable->hasOngoingHand()) {
        for(unsigned int i = 0; i < pokerTable->getCurrentHand()->getCommonCards().size(); i++) {
            drawCard(175 + i * (CARD_WIDTH + 1), 375, pokerTable->getCurrentHand()->getCommonCards().at(i), false, false);
        }
        wostringstream moneyStringStream;
        moneyStringStream << "Pot size: " << pokerTable->getCurrentHand()->getPotSize();
        infoText.setString(moneyStringStream.str());
        infoText.setPosition(150, 300);
        window->draw(infoText);
    }

    for(int i = 0; i < pokerTable->getPlayerCount(); i++) {
            //distribute the players in a circle around the table
        int locationX = playerLocations[i*2];
        int locationY = playerLocations[i*2+1];
        Player* player = pokerTable->getPlayers().at(i);

        //draw the cards if a hand is in progress and the current player was in the hand
        if(pokerTable->hasOngoingHand() && pokerTable->getCurrentHand()->inHand(player)) {
            bool currentPlayer = pokerTable->getCurrentHand()->anyPlayerMayAct() && player == pokerTable->getCurrentHand()->getNextPlayer();

            if(i == pokerTable->getSmallBlindLocation()) {
                smallBlindMarker.setPosition(locationX,  locationY - 31);
                window->draw(smallBlindMarker);
            }
            if((i - 1) == pokerTable->getSmallBlindLocation() || (i == 0 && pokerTable->getSmallBlindLocation() == pokerTable->getPlayerCount() - 1)) {
                bigBlindMarker.setPosition(locationX,  locationY - 31);
                window->draw(bigBlindMarker);
            }

            ostringstream moneyStringStream;
            moneyStringStream << player->getMoney();
            infoText.setString(moneyStringStream.str());
            infoText.setPosition(locationX + 37, locationY - 31);
            window->draw(infoText);

            ostringstream investedInHandStream;
            investedInHandStream << "In pot: " << " " << player->getInvestedInHand();
            infoText.setString(investedInHandStream.str());
            infoText.setPosition(locationX + 120, locationY - 31);
            window->draw(infoText);

                //for AI players draw the card's backsides
            if(player->isAIControlled() && !pokerTable->getCurrentHand()->isOver()) {
                if(player->hasFolded()) {
                    cardBackside.setColor(foldedColor);
                } else if(currentPlayer) {
                    cardBackside.setColor(currentPlayerColor);
                }
                cardBackside.setPosition(sf::Vector2f(locationX, locationY));
                window->draw(cardBackside);
                cardBackside.setPosition(sf::Vector2f(locationX + CARD_WIDTH + 1, locationY));
                window->draw(cardBackside);
                cardBackside.setColor(sf::Color::White);
                //for human players show their cards
            } else {
                drawCard(locationX, locationY, player->getCard1(), player->hasFolded(), currentPlayer);
                drawCard(locationX + CARD_WIDTH + 1, locationY, player->getCard2(), player->hasFolded(), currentPlayer);
            }
        }
    }
}

bool PokerTableRenderer::actionButtonsActivated() {
    return pokerTable->hasOngoingHand()
       && pokerTable->getCurrentHand()->anyPlayerMayAct()
       && !pokerTable->getCurrentHand()->getNextPlayer()->isAIControlled();
}

bool PokerTableRenderer::nextHandButtonActivated() {
    return pokerTable->hasOngoingHand() && pokerTable->getCurrentHand()->isOver();
}

void PokerTableRenderer::drawInputControls() {
    wostringstream infoTextStream;
    infoTextStream << L"Ο χρόνος διδάσκαλος των ανθρωπον εστίν";

    if(actionButtonsActivated()) {
        window->draw(foldButtonSprite);
        window->draw(callCheckButtonSprite);
        window->draw(betButtonSprite);
    }
    if(nextHandButtonActivated()) {
        infoText.setColor(sf::Color::Red);
        infoText.setPosition(BUTTON_X, NEXT_HAND_BUTTON_Y - 93);
        infoText.setString(infoTextStream.str().substr(0, 20));
        window->draw(infoText);
        infoText.setPosition(BUTTON_X, NEXT_HAND_BUTTON_Y - 62);
        infoText.setString(infoTextStream.str().substr(20));
        window->draw(infoText);

        infoText.setColor(sf::Color::Black);

        window->draw(nextHandSprite);
    }
}

void PokerTableRenderer::handleLeftMouseClick(sf::Event &event) {
    int mouseX = sf::Mouse::getPosition(*window).x;
    int mouseY = sf::Mouse::getPosition(*window).y;

    if(actionButtonsActivated()) {
        //fold button pressed
        if(mouseX > BUTTON_X && mouseX < BUTTON_X + BUTTON_WIDTH && mouseY > FOLD_BUTTON_Y && mouseY < FOLD_BUTTON_Y + BUTTON_HEIGHT) {
            pokerTable->currentPlayerFolded();
        }
        //check/call button pressed
        if(mouseX > BUTTON_X && mouseX < BUTTON_X + BUTTON_WIDTH && mouseY > CHECK_BUTTON_Y && mouseY < CHECK_BUTTON_Y + BUTTON_HEIGHT) {
            if(pokerTable->hasOngoingHand() && !pokerTable->getCurrentHand()->isOver()) {
                pokerTable->currentPlayerCalled();
            }
        }
        if(mouseX > BUTTON_X && mouseX < BUTTON_X + BUTTON_WIDTH && mouseY > BET_BUTTON_Y && mouseY < BET_BUTTON_Y + BUTTON_HEIGHT) {
            pokerTable->currentPlayerBetted();
        }
    }
    if(nextHandButtonActivated()) {
        if(mouseX > BUTTON_X && mouseX < BUTTON_X + BUTTON_WIDTH && mouseY > NEXT_HAND_BUTTON_Y && mouseY < NEXT_HAND_BUTTON_Y + BUTTON_HEIGHT) {
            pokerTable->allowToProceedToNextHand();
        }
    }
}

void PokerTableRenderer::draw() {
    window->clear();

    drawBoard();
    drawInputControls();

    window->display();
}

void PokerTableRenderer::drawCard(int x, int y, Card* card, bool folded, bool currentPlayer) {
    int spriteOffset = 0;
    if(card->getSuit() == Card::SPADES) {
        spriteOffset = 13;
    } else if (card->getSuit() == Card::DIAMONDS) {
        spriteOffset = 26;
    } else if (card->getSuit() == Card::HEARTS) {
        spriteOffset = 39;
    }
    const int arrayIndex = spriteOffset + card->getRank() - 2;
    cardSprites[arrayIndex].setPosition(sf::Vector2f(x, y));
    if(folded) {
        cardSprites[arrayIndex].setColor(foldedColor);
    }
    if(currentPlayer) {
        cardSprites[arrayIndex].setColor(currentPlayerColor);
    }
    window->draw(cardSprites[arrayIndex]);
    cardSprites[arrayIndex].setColor(sf::Color::White);
}
