#include <vector>

#include "HandValue.h"
#include "Card.h"
#include "HandEvaluator.h"

#include <iostream>

using std::cout;
using std::cin;
using std::vector;

vector<Card*> straightFlush;
vector<Card*> straightFlushWheel;
vector<Card*> flush;
vector<Card*> straight;
vector<Card*> straightWheel;
vector<Card*> four_of_a_kind;
vector<Card*> fullHouse;
vector<Card*> fullHouseBigPair;
vector<Card*> fullHouseBetterTrips;

vector<Card*> three_of_a_kind;
vector<Card*> three_of_a_kind_high_kicker;
vector<Card*> three_of_a_kind_high_second_kicker;
vector<Card*> aPair;
vector<Card*> aPairGoodThirdKicker;
vector<Card*> aPairGoodFourthKicker;
vector<Card*> twoPairs;
vector<Card*> twoPairsBetterHigh;
vector<Card*> twoPairsHighKicker;
vector<Card*> highCard;
vector<Card*> highCardGoodFifthKicker;
vector<Card*> highCardGoodSixthKicker;

//found bugs during play
vector<Card*> bestTwoPairs;
vector<Card*> secondBestTwoPairs;
vector<Card*> thirdBestTwoPairs;

vector<Card*> badThreeCardHand;
vector<Card*> goodThreeCardHand;

vector<Card*> badFourCardHand;
vector<Card*> goodFourCardHand;


bool handsAreRecognized(HandEvaluator* handEvaluator) {
   if(handEvaluator->getHandValue(flush).getHandType() != HandValue::FLUSH) {
        return false;
    }
    if(handEvaluator->getHandValue(straight).getHandType() != HandValue::STRAIGHT) {
        return false;
    }
    if(handEvaluator->getHandValue(straightWheel).getHandType() != HandValue::STRAIGHT) {
        return false;
    }
    if(handEvaluator->getHandValue(straightFlush).getHandType() != HandValue::STRAIGHT_FLUSH) {
        return false;
    }
    if(handEvaluator->getHandValue(straightFlushWheel).getHandType() != HandValue::STRAIGHT_FLUSH) {
        return false;
    }
    if(handEvaluator->getHandValue(aPair).getHandType() != HandValue::PAIR) {
        return false;
    }
    if(handEvaluator->getHandValue(twoPairs).getHandType() != HandValue::TWO_PAIRS) {
        return false;
    }
    if(handEvaluator->getHandValue(three_of_a_kind).getHandType() != HandValue::THREE_OF_A_KIND) {
        return false;
    }
    if(handEvaluator->getHandValue(fullHouse).getHandType() != HandValue::FULL_HOUSE) {
        return false;
    }
    if(handEvaluator->getHandValue(four_of_a_kind).getHandType() != HandValue::FOUR_OF_A_KIND) {
        return false;
    }
    if(handEvaluator->getHandValue(highCard).getHandType() != HandValue::HIGH_CARD) {
        return false;
    }

    if(handEvaluator->getHandValue(badThreeCardHand).getHandType() != HandValue::HIGH_CARD) {
        return false;
    }
    if(handEvaluator->getHandValue(badFourCardHand).getHandType() != HandValue::HIGH_CARD) {
        return false;
    }
    if(handEvaluator->getHandValue(goodThreeCardHand).getHandType() != HandValue::THREE_OF_A_KIND) {
        return false;
    }
    if(handEvaluator->getHandValue(goodFourCardHand).getHandType() != HandValue::TWO_PAIRS) {
        return false;
    }

    return true;
}

bool flushBeatsThreeOfAKind(HandEvaluator* handEvaluator) {
    if(handEvaluator->getHandValue(flush).equalTo(handEvaluator->getHandValue(straight))) {
        return false;
    }
    if(handEvaluator->getHandValue(flush).worseThan(handEvaluator->getHandValue(straight))) {
        return false;
    }
    return handEvaluator->getHandValue(flush).betterThan(handEvaluator->getHandValue(straight));
}

bool straightBeatsWheel(HandEvaluator* handEvaluator) {
    if(handEvaluator->getHandValue(straight).equalTo(handEvaluator->getHandValue(straightWheel))) {
        return false;
    }
    if(handEvaluator->getHandValue(straight).worseThan(handEvaluator->getHandValue(straightWheel))) {
        return false;
    }
    return handEvaluator->getHandValue(straight).betterThan(handEvaluator->getHandValue(straightWheel));
}

bool thirdKickerMathers(HandEvaluator* handEvaluator) {
    if(handEvaluator->getHandValue(aPairGoodThirdKicker).equalTo(handEvaluator->getHandValue(aPair))) {
        return false;
    }
    if(handEvaluator->getHandValue(aPairGoodThirdKicker).worseThan(handEvaluator->getHandValue(aPair))) {
        return false;
    }
    return handEvaluator->getHandValue(aPairGoodThirdKicker).betterThan(handEvaluator->getHandValue(aPair));
}

bool fourthKickerDoesNotMatter(HandEvaluator* handEvaluator) {
    if(handEvaluator->getHandValue(aPairGoodFourthKicker).worseThan(handEvaluator->getHandValue(aPair))) {
        return false;
    }
    if(handEvaluator->getHandValue(aPairGoodFourthKicker).betterThan(handEvaluator->getHandValue(aPair))) {
        return false;
    }
    return handEvaluator->getHandValue(aPairGoodFourthKicker).equalTo(handEvaluator->getHandValue(aPair));
}

bool sixthKickerDoNotMatter(HandEvaluator* handEvaluator) {
    if(handEvaluator->getHandValue(highCardGoodSixthKicker).worseThan(handEvaluator->getHandValue(highCard))) {
        return false;
    }
    if(handEvaluator->getHandValue(highCardGoodSixthKicker).betterThan(handEvaluator->getHandValue(highCard))) {
        return false;
    }
    return handEvaluator->getHandValue(highCardGoodSixthKicker).equalTo(handEvaluator->getHandValue(highCard));
}

bool fifthKickerDoMatter(HandEvaluator* handEvaluator) {
    if(handEvaluator->getHandValue(highCardGoodFifthKicker).equalTo(handEvaluator->getHandValue(highCard))) {
        return false;
    }
    if(handEvaluator->getHandValue(highCardGoodFifthKicker).worseThan(handEvaluator->getHandValue(highCard))) {
        return false;
    }
    return handEvaluator->getHandValue(highCardGoodFifthKicker).betterThan(handEvaluator->getHandValue(highCard));
}

bool fullHouseValuingCorrect(HandEvaluator* handEvaluator) {
    if(handEvaluator->getHandValue(fullHouseBetterTrips).equalTo(handEvaluator->getHandValue(fullHouse))) {
        return false;
    }
    if(handEvaluator->getHandValue(fullHouseBetterTrips).worseThan(handEvaluator->getHandValue(fullHouse))) {
        return false;
    }
    if (!handEvaluator->getHandValue(fullHouseBetterTrips).betterThan(handEvaluator->getHandValue(fullHouse))) {
        return false;
    }

    if(handEvaluator->getHandValue(fullHouseBigPair).equalTo(handEvaluator->getHandValue(fullHouse))) {
        return false;
    }
    if(handEvaluator->getHandValue(fullHouseBigPair).betterThan(handEvaluator->getHandValue(fullHouse))) {
        return false;
    }
    if (!handEvaluator->getHandValue(fullHouseBigPair).worseThan(handEvaluator->getHandValue(fullHouse))) {
        return false;
    }

    if(handEvaluator->getHandValue(fullHouseBigPair).equalTo(handEvaluator->getHandValue(fullHouseBetterTrips))) {
        return false;
    }
    if(handEvaluator->getHandValue(fullHouseBigPair).betterThan(handEvaluator->getHandValue(fullHouseBetterTrips))) {
        return false;
    }
    if (!handEvaluator->getHandValue(fullHouseBigPair).worseThan(handEvaluator->getHandValue(fullHouseBetterTrips))) {
        return false;
    }
    return true;
}

bool twoPairOrderingCorrect(HandEvaluator* handEvaluator) {
    if(handEvaluator->getHandValue(twoPairsBetterHigh).equalTo(handEvaluator->getHandValue(twoPairs))) {
        return false;
    }
    if(handEvaluator->getHandValue(twoPairsBetterHigh).worseThan(handEvaluator->getHandValue(twoPairs))) {
        return false;
    }
    if (!handEvaluator->getHandValue(twoPairsBetterHigh).betterThan(handEvaluator->getHandValue(twoPairs))) {
        return false;
    }

    if(handEvaluator->getHandValue(twoPairsHighKicker).equalTo(handEvaluator->getHandValue(twoPairs))) {
        return false;
    }
    if(handEvaluator->getHandValue(twoPairsHighKicker).worseThan(handEvaluator->getHandValue(twoPairs))) {
        return false;
    }
    if (!handEvaluator->getHandValue(twoPairsHighKicker).betterThan(handEvaluator->getHandValue(twoPairs))) {
        return false;
    }

    if(handEvaluator->getHandValue(twoPairsHighKicker).equalTo(handEvaluator->getHandValue(twoPairsBetterHigh))) {
        return false;
    }
    if(handEvaluator->getHandValue(twoPairsHighKicker).betterThan(handEvaluator->getHandValue(twoPairsBetterHigh))) {
        return false;
    }
    if (!handEvaluator->getHandValue(twoPairsHighKicker).worseThan(handEvaluator->getHandValue(twoPairsBetterHigh))) {
        return false;
    }
    return true;
}


int main() {
    HandEvaluator* handEvaluator = new HandEvaluator();

    flush.push_back(new Card(5, Card::HEARTS));
    flush.push_back(new Card(6, Card::HEARTS));
    flush.push_back(new Card(5, Card::CLUBS));
    flush.push_back(new Card(7, Card::HEARTS));
    flush.push_back(new Card(5, Card::SPADES));
    flush.push_back(new Card(8, Card::HEARTS));
    flush.push_back(new Card(10, Card::HEARTS));

    straightFlush.push_back(new Card(5, Card::HEARTS));
    straightFlush.push_back(new Card(6, Card::HEARTS));
    straightFlush.push_back(new Card(5, Card::CLUBS));
    straightFlush.push_back(new Card(7, Card::HEARTS));
    straightFlush.push_back(new Card(5, Card::SPADES));
    straightFlush.push_back(new Card(8, Card::HEARTS));
    straightFlush.push_back(new Card(9, Card::HEARTS));

    straightFlushWheel.push_back(new Card(5, Card::HEARTS));
    straightFlushWheel.push_back(new Card(3, Card::HEARTS));
    straightFlushWheel.push_back(new Card(5, Card::CLUBS));
    straightFlushWheel.push_back(new Card(4, Card::HEARTS));
    straightFlushWheel.push_back(new Card(5, Card::SPADES));
    straightFlushWheel.push_back(new Card(2, Card::HEARTS));
    straightFlushWheel.push_back(new Card(14, Card::HEARTS));

    straightWheel.push_back(new Card(5, Card::SPADES));
    straightWheel.push_back(new Card(3, Card::HEARTS));
    straightWheel.push_back(new Card(5, Card::CLUBS));
    straightWheel.push_back(new Card(4, Card::HEARTS));
    straightWheel.push_back(new Card(5, Card::SPADES));
    straightWheel.push_back(new Card(2, Card::HEARTS));
    straightWheel.push_back(new Card(14, Card::HEARTS));

    straight.push_back(new Card(5, Card::SPADES));
    straight.push_back(new Card(3, Card::HEARTS));
    straight.push_back(new Card(5, Card::CLUBS));
    straight.push_back(new Card(4, Card::HEARTS));
    straight.push_back(new Card(5, Card::SPADES));
    straight.push_back(new Card(2, Card::HEARTS));
    straight.push_back(new Card(6, Card::HEARTS));


    aPair.push_back(new Card(2, Card::SPADES));
    aPair.push_back(new Card(3, Card::HEARTS));
    aPair.push_back(new Card(4, Card::CLUBS));
    aPair.push_back(new Card(7, Card::HEARTS));
    aPair.push_back(new Card(9, Card::SPADES));
    aPair.push_back(new Card(10, Card::HEARTS));
    aPair.push_back(new Card(2, Card::HEARTS));

    aPairGoodThirdKicker.push_back(new Card(2, Card::SPADES));
    aPairGoodThirdKicker.push_back(new Card(3, Card::HEARTS));
    aPairGoodThirdKicker.push_back(new Card(4, Card::CLUBS));
    aPairGoodThirdKicker.push_back(new Card(8, Card::HEARTS));
    aPairGoodThirdKicker.push_back(new Card(9, Card::SPADES));
    aPairGoodThirdKicker.push_back(new Card(10, Card::HEARTS));
    aPairGoodThirdKicker.push_back(new Card(2, Card::HEARTS));

    aPairGoodFourthKicker.push_back(new Card(2, Card::SPADES));
    aPairGoodFourthKicker.push_back(new Card(3, Card::HEARTS));
    aPairGoodFourthKicker.push_back(new Card(6, Card::CLUBS));
    aPairGoodFourthKicker.push_back(new Card(7, Card::HEARTS));
    aPairGoodFourthKicker.push_back(new Card(9, Card::SPADES));
    aPairGoodFourthKicker.push_back(new Card(10, Card::HEARTS));
    aPairGoodFourthKicker.push_back(new Card(2, Card::HEARTS));

    three_of_a_kind.push_back(new Card(3, Card::SPADES));
    three_of_a_kind.push_back(new Card(3, Card::HEARTS));
    three_of_a_kind.push_back(new Card(3, Card::CLUBS));
    three_of_a_kind.push_back(new Card(2, Card::HEARTS));
    three_of_a_kind.push_back(new Card(5, Card::SPADES));
    three_of_a_kind.push_back(new Card(8, Card::HEARTS));
    three_of_a_kind.push_back(new Card(11, Card::HEARTS));

    four_of_a_kind.push_back(new Card(3, Card::SPADES));
    four_of_a_kind.push_back(new Card(3, Card::HEARTS));
    four_of_a_kind.push_back(new Card(3, Card::CLUBS));
    four_of_a_kind.push_back(new Card(3, Card::HEARTS));
    four_of_a_kind.push_back(new Card(5, Card::SPADES));
    four_of_a_kind.push_back(new Card(6, Card::HEARTS));
    four_of_a_kind.push_back(new Card(7, Card::HEARTS));

    fullHouse.push_back(new Card(6, Card::SPADES));
    fullHouse.push_back(new Card(6, Card::HEARTS));
    fullHouse.push_back(new Card(6, Card::CLUBS));
    fullHouse.push_back(new Card(3, Card::HEARTS));
    fullHouse.push_back(new Card(3, Card::SPADES));
    fullHouse.push_back(new Card(8, Card::HEARTS));
    fullHouse.push_back(new Card(11, Card::HEARTS));

    fullHouseBigPair.push_back(new Card(5, Card::SPADES));
    fullHouseBigPair.push_back(new Card(5, Card::HEARTS));
    fullHouseBigPair.push_back(new Card(5, Card::CLUBS));
    fullHouseBigPair.push_back(new Card(13, Card::HEARTS));
    fullHouseBigPair.push_back(new Card(13, Card::SPADES));
    fullHouseBigPair.push_back(new Card(8, Card::HEARTS));
    fullHouseBigPair.push_back(new Card(11, Card::HEARTS));

    fullHouseBetterTrips.push_back(new Card(7, Card::SPADES));
    fullHouseBetterTrips.push_back(new Card(7, Card::HEARTS));
    fullHouseBetterTrips.push_back(new Card(7, Card::CLUBS));
    fullHouseBetterTrips.push_back(new Card(2, Card::HEARTS));
    fullHouseBetterTrips.push_back(new Card(2, Card::SPADES));
    fullHouseBetterTrips.push_back(new Card(8, Card::HEARTS));
    fullHouseBetterTrips.push_back(new Card(11, Card::HEARTS));

    twoPairs.push_back(new Card(7, Card::SPADES));
    twoPairs.push_back(new Card(7, Card::HEARTS));
    twoPairs.push_back(new Card(9, Card::CLUBS));
    twoPairs.push_back(new Card(5, Card::HEARTS));
    twoPairs.push_back(new Card(5, Card::SPADES));
    twoPairs.push_back(new Card(8, Card::HEARTS));
    twoPairs.push_back(new Card(11, Card::HEARTS));


    twoPairsBetterHigh.push_back(new Card(8, Card::SPADES));
    twoPairsBetterHigh.push_back(new Card(8, Card::HEARTS));
    twoPairsBetterHigh.push_back(new Card(9, Card::CLUBS));
    twoPairsBetterHigh.push_back(new Card(2, Card::HEARTS));
    twoPairsBetterHigh.push_back(new Card(2, Card::SPADES));
    twoPairsBetterHigh.push_back(new Card(8, Card::HEARTS));
    twoPairsBetterHigh.push_back(new Card(11, Card::HEARTS));


    twoPairsHighKicker.push_back(new Card(7, Card::SPADES));
    twoPairsHighKicker.push_back(new Card(7, Card::HEARTS));
    twoPairsHighKicker.push_back(new Card(14, Card::CLUBS));
    twoPairsHighKicker.push_back(new Card(5, Card::HEARTS));
    twoPairsHighKicker.push_back(new Card(5, Card::SPADES));
    twoPairsHighKicker.push_back(new Card(8, Card::HEARTS));
    twoPairsHighKicker.push_back(new Card(11, Card::HEARTS));

    highCard.push_back(new Card(2, Card::SPADES));
    highCard.push_back(new Card(4, Card::HEARTS));
    highCard.push_back(new Card(6, Card::CLUBS));
    highCard.push_back(new Card(8, Card::HEARTS));
    highCard.push_back(new Card(10, Card::SPADES));
    highCard.push_back(new Card(12, Card::HEARTS));
    highCard.push_back(new Card(14, Card::HEARTS));

    highCardGoodFifthKicker.push_back(new Card(2, Card::SPADES));
    highCardGoodFifthKicker.push_back(new Card(4, Card::HEARTS));
    highCardGoodFifthKicker.push_back(new Card(7, Card::CLUBS));
    highCardGoodFifthKicker.push_back(new Card(8, Card::HEARTS));
    highCardGoodFifthKicker.push_back(new Card(10, Card::SPADES));
    highCardGoodFifthKicker.push_back(new Card(12, Card::HEARTS));
    highCardGoodFifthKicker.push_back(new Card(14, Card::HEARTS));

    highCardGoodSixthKicker.push_back(new Card(2, Card::SPADES));
    highCardGoodSixthKicker.push_back(new Card(5, Card::HEARTS));
    highCardGoodSixthKicker.push_back(new Card(6, Card::CLUBS));
    highCardGoodSixthKicker.push_back(new Card(8, Card::HEARTS));
    highCardGoodSixthKicker.push_back(new Card(10, Card::SPADES));
    highCardGoodSixthKicker.push_back(new Card(12, Card::HEARTS));
    highCardGoodSixthKicker.push_back(new Card(14, Card::HEARTS));



    bestTwoPairs.push_back(new Card(9, Card::CLUBS));
    bestTwoPairs.push_back(new Card(5, Card::DIAMONDS));
    bestTwoPairs.push_back(new Card(13, Card::HEARTS));
    bestTwoPairs.push_back(new Card(13, Card::DIAMONDS));
    bestTwoPairs.push_back(new Card(2, Card::CLUBS));
    bestTwoPairs.push_back(new Card(9, Card::SPADES));
    bestTwoPairs.push_back(new Card(2, Card::SPADES));

    secondBestTwoPairs.push_back(new Card(9, Card::HEARTS));
    secondBestTwoPairs.push_back(new Card(4, Card::SPADES));
    secondBestTwoPairs.push_back(new Card(13, Card::HEARTS));
    secondBestTwoPairs.push_back(new Card(13, Card::DIAMONDS));
    secondBestTwoPairs.push_back(new Card(2, Card::CLUBS));
    secondBestTwoPairs.push_back(new Card(9, Card::SPADES));
    secondBestTwoPairs.push_back(new Card(2, Card::SPADES));

    thirdBestTwoPairs.push_back(new Card(14, Card::HEARTS));
    thirdBestTwoPairs.push_back(new Card(5, Card::SPADES));
    thirdBestTwoPairs.push_back(new Card(13, Card::HEARTS));
    thirdBestTwoPairs.push_back(new Card(13, Card::DIAMONDS));
    thirdBestTwoPairs.push_back(new Card(2, Card::CLUBS));
    thirdBestTwoPairs.push_back(new Card(9, Card::SPADES));
    thirdBestTwoPairs.push_back(new Card(2, Card::SPADES));

    badThreeCardHand.push_back(new Card(2, Card::HEARTS));
    badThreeCardHand.push_back(new Card(3, Card::HEARTS));
    badThreeCardHand.push_back(new Card(4, Card::HEARTS));

    badFourCardHand.push_back(new Card(2, Card::HEARTS));
    badFourCardHand.push_back(new Card(3, Card::HEARTS));
    badFourCardHand.push_back(new Card(4, Card::HEARTS));
    badFourCardHand.push_back(new Card(5, Card::HEARTS));

    goodThreeCardHand.push_back(new Card(2, Card::HEARTS));
    goodThreeCardHand.push_back(new Card(2, Card::CLUBS));
    goodThreeCardHand.push_back(new Card(2, Card::SPADES));

    goodFourCardHand.push_back(new Card(2, Card::HEARTS));
    goodFourCardHand.push_back(new Card(2, Card::CLUBS));
    goodFourCardHand.push_back(new Card(3, Card::SPADES));
    goodFourCardHand.push_back(new Card(3, Card::DIAMONDS));


    bool hasError = false;
    if(!flushBeatsThreeOfAKind(handEvaluator)) {
        hasError = true;
    }

    if(!handsAreRecognized(handEvaluator)) {
        hasError = true;
    }

    if(!straightBeatsWheel(handEvaluator)) {
        hasError = true;
    }

    if(!thirdKickerMathers(handEvaluator)) {
        hasError = true;
    }

    if(!fourthKickerDoesNotMatter(handEvaluator)) {
        hasError = true;
    }

    if(!fifthKickerDoMatter(handEvaluator)) {
        hasError = true;
    }

    if(!sixthKickerDoNotMatter(handEvaluator)) {
        hasError = true;
    }

    if(!fullHouseValuingCorrect(handEvaluator)) {
        hasError = true;
    }

    if(!twoPairOrderingCorrect(handEvaluator)) {
        hasError = true;
    }

    //recreating bug seen in game play
    HandValue bestTwoPairsValue = handEvaluator->getHandValue(bestTwoPairs);
    HandValue secondBestTwoPairsValue = handEvaluator->getHandValue(secondBestTwoPairs);
    HandValue thirdBestTwoPairsValue = handEvaluator->getHandValue(thirdBestTwoPairs);
    if(!bestTwoPairsValue.betterThan(secondBestTwoPairsValue)) {
        hasError = true;
    }
    if(!bestTwoPairsValue.betterThan(thirdBestTwoPairsValue)) {
        hasError = true;
    }
    if(!secondBestTwoPairsValue.betterThan(thirdBestTwoPairsValue)) {
        hasError = true;
    }

    if(!hasError) {
        cout << "All tests passed!";
    }

    int delay = 0;
    cin >> delay;
    return 0;
}
