#include "Pot.h"

Pot::Pot(HandEvaluator* handEvaluator) : handEvaluator(handEvaluator)
{
    potSize = 0;
}

Pot::~Pot()
{
    //dtor
}

void Pot::decidePot(vector<Card*> commonCards) {
    vector<Player*> winners;
    vector<Card*> sevenCardHand = commonCards;
    sevenCardHand.push_back(players.at(0)->getCard1());
    sevenCardHand.push_back(players.at(0)->getCard2());
    winners.push_back(players.at(0));
    HandValue bestHandValue = handEvaluator->getHandValue(sevenCardHand);
    for(unsigned int i = 1; i < players.size(); i++) {
        sevenCardHand.clear();
        sevenCardHand = commonCards;
        sevenCardHand.push_back(players.at(i)->getCard1());
        sevenCardHand.push_back(players.at(i)->getCard2());
        HandValue handValue = handEvaluator->getHandValue(sevenCardHand);
        if(handValue.equalTo(bestHandValue)) {
            winners.push_back(players.at(i));
        } else if(handValue.betterThan(bestHandValue)) {
            bestHandValue = handValue;
            for(unsigned int j = 0; j < winners.size(); j++) {
                    //no longer a winner. learn from last hand with negative reward
                if(winners.at(j)->isAIControlled()) {
                    winners.at(j)->getAI()->learnFromLastHand(-winners.at(j)->getInvestedInHand());
                }
            }
            winners.clear();
            winners.push_back(players.at(i));
        } else if(handValue.worseThan(bestHandValue)) {
            //never a winner. learn from last hand with negative reward
            if(players.at(i)->isAIControlled()) {
                players.at(i)->getAI()->learnFromLastHand(-players.at(i)->getInvestedInHand());
            }
        }
    }
    double potShare = getPotSize() / winners.size();
    for(unsigned int i = 0; i < winners.size(); i++) {
        winners.at(i)->addMoney(potShare);
        //let winners learn from the last hand
        if(winners.at(i)->isAIControlled()) {
            winners.at(i)->getAI()->learnFromLastHand(potShare - winners.at(i)->getInvestedInHand());
        }
    }
}

void Pot::addChips(double amount) {
    potSize += amount;
}
void Pot::addPlayer(Player* player) {
    players.push_back(player);
}


double Pot::getPotSize() {
    return potSize;
}
