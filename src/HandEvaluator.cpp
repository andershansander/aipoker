#include "HandEvaluator.h"

#include <map>
#include <vector>
#include <algorithm>

#include "HandValue.h"


using std::map;
using std::vector;

HandEvaluator::HandEvaluator()
{
    //ctor
}

HandEvaluator::~HandEvaluator()
{
    //dtor
}

HandValue HandEvaluator::get5CardHandValue(vector<Card*> cards) {
    vector<int> sortedRanks;
    for(unsigned int i=0; i < cards.size(); i++) {
        sortedRanks.push_back(cards.at(i)->getRank());
    }
    std::sort(sortedRanks.begin(), sortedRanks.end());
    std::reverse(sortedRanks.begin(),sortedRanks.end());

    map<int, int> rankOccurences;
    int highestOccurence = 0;
    int secondHighestOccurence = 0;
    int highestOccurenceRank = 0;
    int secondHighestOccurenceRank = 0;
    for(unsigned int i = 0; i < cards.size(); i++) {
        int rank = cards.at(i)->getRank();
        if(rankOccurences.find(rank) == rankOccurences.end()) {
            rankOccurences[rank] = 1;
        } else {
            rankOccurences[rank]++;
        }
        if(rankOccurences[rank] > highestOccurence) {
            secondHighestOccurenceRank = highestOccurenceRank;
            secondHighestOccurence = highestOccurence;
            highestOccurence = rankOccurences[rank];
            highestOccurenceRank = rank;
        }
        else if(rankOccurences[rank] > secondHighestOccurence) {
            secondHighestOccurence = rankOccurences[rank];
            secondHighestOccurenceRank = rank;
        }
    }

    if(highestOccurence == 4) {
        HandValue result(HandValue::FOUR_OF_A_KIND);
        result.addKickerValue(highestOccurenceRank);
        result.addKickerValue(secondHighestOccurenceRank);
        return result;
    }
    if(highestOccurence == 3 && rankOccurences.size() == 2 && cards.size() == 5) {
        HandValue result(HandValue::FULL_HOUSE);
        result.addKickerValue(highestOccurenceRank);
        result.addKickerValue(secondHighestOccurenceRank);
        return result;
    }
    if(highestOccurence == 3) {
        HandValue result(HandValue::THREE_OF_A_KIND);
        result.addKickerValue(highestOccurenceRank);
        for(unsigned int i = 0; i < cards.size(); i++) {
            if(sortedRanks.at(i) != highestOccurenceRank) {
                result.addKickerValue(sortedRanks.at(i));
            }
        }
        return result;
    }
    if(highestOccurence == 2 && cards.size() >= 4 && rankOccurences.size() == cards.size() - 2) {
        HandValue result(HandValue::TWO_PAIRS);
        int highestPairRank;
        int lowerPairRank;
        if(highestOccurenceRank > secondHighestOccurenceRank) {
            highestPairRank = highestOccurenceRank;
            lowerPairRank = secondHighestOccurenceRank;
        } else {
            highestPairRank = secondHighestOccurenceRank;
            lowerPairRank = highestOccurenceRank;
        }
        result.addKickerValue(highestPairRank);
        result.addKickerValue(lowerPairRank);

        for(unsigned int i = 0; i < cards.size(); i++) {
            if(sortedRanks.at(i) != highestOccurenceRank && sortedRanks.at(i) != secondHighestOccurenceRank) {
                result.addKickerValue(sortedRanks.at(i));
            }
        }
        return result;
    }
    if(highestOccurence == 2 && rankOccurences.size() == cards.size() - 1) {
        HandValue result(HandValue::PAIR);
        result.addKickerValue(highestOccurenceRank);
        for(unsigned int i = 0; i < cards.size(); i++) {
            if(sortedRanks.at(i) != highestOccurenceRank) {
                result.addKickerValue(sortedRanks.at(i));
            }
        }
        return result;
    }

    bool flush = true && cards.size() == 5;
    Card::Suit firstCardSuit = cards.at(0)->getSuit();
    for(unsigned int i = 1; i < cards.size(); i++) {
        if(cards.at(i)->getSuit() != firstCardSuit) {
            flush = false;
            break;
        }
    }

    bool straight = false;
    //if regular straight or the wheel
    if(cards.size() == 5 && (sortedRanks.at(0) - sortedRanks.at(4) == 4 || (sortedRanks.at(0) == 14 && sortedRanks.at(1) == 5 && sortedRanks.at(4) == 2))) {
        straight = true;
    }

    if(straight && flush) {
        HandValue result(HandValue::STRAIGHT_FLUSH);
        if(sortedRanks.at(0) != 14) {
            result.addKickerValue(sortedRanks.at(0));
        } else {
            result.addKickerValue(sortedRanks.at(1));
        }

        return result;
    } else if(straight) {
        HandValue result(HandValue::STRAIGHT);
        if(sortedRanks.at(0) != 14) {
            result.addKickerValue(sortedRanks.at(0));
        } else {
            result.addKickerValue(sortedRanks.at(1));
        }
        return result;
    } else if(flush) {
        HandValue result(HandValue::FLUSH);
        for(int i=0; i < 5; i++) {
            result.addKickerValue(sortedRanks.at(i));
        }
        return result;
    } else {
        HandValue result(HandValue::HIGH_CARD);
        for(unsigned int i=0; i < cards.size(); i++) {
            result.addKickerValue(sortedRanks.at(i));
        }
        return result;
    }
}

HandValue HandEvaluator::getHandValue(vector<Card*> cards) {
    HandValue highestValue(HandValue::HIGH_CARD);
    highestValue.addKickerValue(0);

    if(cards.size() <= 5) {
        return get5CardHandValue(cards);
    } else {
        //remove one card at a time and try to calculate the hand value
        for(unsigned int i = 0; i < cards.size(); i++) {
            Card* removedCard = cards.at(i);
            cards.erase(cards.begin() + i);
            HandValue value = getHandValue(cards);
            if(value.betterThan(highestValue)) {
                highestValue = value;
            }
            cards.insert(cards.begin() + i, removedCard);
        }
    }
    return highestValue;
}

int HandEvaluator::flushCardsInHand(vector<Card*> cards) {
    int maxFlushCards = 0;
    for(int i = 0; i < 4; i++) {
        int flushCards = 0;
        for(unsigned int j=0; j < cards.size(); j++) {
            if(cards.at(j)->getSuit() == i) {
                flushCards++;
            }
        }
        if(flushCards > maxFlushCards) {
            maxFlushCards = flushCards;
        }
    }
    return maxFlushCards;
}
