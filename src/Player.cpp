#include "Player.h"

Player::Player(bool AIControlled, PokerAI* pokerAI) : AIControlled(AIControlled)
{
    this->pokerAI = pokerAI;
    investedInHand = 0;
    money = 0;
}

Player::~Player()
{
    //dtor
}

bool Player::isAIControlled() {
    return AIControlled;
}

Card* Player::getCard1() {
    return card1;
}

Card* Player::getCard2() {
    return card2;
}

bool Player::hasFolded() {
    return folded;
}

void Player::fold() {
    folded = true;
}

void Player::giveNewHand(Card* card1, Card* card2) {
    this->card1 = card1;
    this->card2 = card2;
    folded = false;
    investedInHand = 0;
}

void Player::addInvestedInHand(double amount) {
    if(amount > money) {
        amount = money;
    }
    money -= amount;
    investedInHand += amount;
}

double Player::getInvestedInHand() {
    return investedInHand;
}

double Player::getMoney() {
    return money;
}

void Player::addMoney(double amount) {
    money += amount;
}

bool Player::isAllIn() {
    return !folded && money == 0;
}

PokerAI* Player::getAI() {
    return pokerAI;
}
