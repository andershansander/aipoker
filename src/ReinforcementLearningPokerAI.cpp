#include "ReinforcementLearningPokerAI.h"

#include <iterator>
#include <iostream>
#include <fstream>
#include <cstdlib>

using std::ofstream;
using std::ifstream;

ReinforcementLearningPokerAI::ReinforcementLearningPokerAI(double learningRate, double discountFactor, double maximumExpectedActionValue) :
    learningRate(learningRate), discountFactor(discountFactor), maximumExpectedActionValue(maximumExpectedActionValue)
{
    //ctor
}

ReinforcementLearningPokerAI::~ReinforcementLearningPokerAI()
{
    //dtor
}

Action* ReinforcementLearningPokerAI::decideNextPreFlopAction(string situationKey, double betSize, bool canCheck, bool canBet) {
    if(preFlopActionMap.find(situationKey) == preFlopActionMap.end())
    {
        preFlopActionMap[situationKey] = new ActionEvaluation(learningRate, discountFactor, maximumExpectedActionValue);
    }

    Action::Type nextActionType = preFlopActionMap[situationKey]->getNextActionToTry(canCheck, canBet);
    if(preFlopActionHistory.find(situationKey) == preFlopActionHistory.end()) {
        vector<Action::Type>* decisionVector = new vector<Action::Type>();
        preFlopActionHistory[situationKey] = decisionVector;
    }
    preFlopActionHistory[situationKey]->push_back(nextActionType);
    if(nextActionType == Action::BET) {
        return new Action(nextActionType, betSize);
    } else {
        return new Action(nextActionType, 0);
    }
}

Action* ReinforcementLearningPokerAI::decideNextPostFlopAction(string situationKey, double betSize, bool canCheck, bool canBet) {
    if(postFlopActionMap.find(situationKey) == postFlopActionMap.end())
    {
        postFlopActionMap[situationKey] = new ActionEvaluation(learningRate, discountFactor, maximumExpectedActionValue);
    }

    Action::Type nextActionType = postFlopActionMap[situationKey]->getNextActionToTry(canCheck, canBet);
    if(postFlopActionHistory.find(situationKey) == postFlopActionHistory.end()) {
        vector<Action::Type>* decisionVector = new vector<Action::Type>();
        postFlopActionHistory[situationKey] = decisionVector;
    }
    postFlopActionHistory[situationKey]->push_back(nextActionType);
    if(nextActionType == Action::BET) {
        return new Action(nextActionType, betSize);
    } else {
        return new Action(nextActionType, 0);
    }
}

void ReinforcementLearningPokerAI::learnFromLastHand(double reward) {
    if(preFlopActionHistory.size() + postFlopActionHistory.size() == 0) {
        return;
    }
    int numberActionsInHand = 0;
    for(map<string, vector<Action::Type>*>::iterator iterator = preFlopActionHistory.begin(); iterator != preFlopActionHistory.end(); iterator++) {
        numberActionsInHand += preFlopActionHistory[iterator->first]->size();
    }

    for(map<string, vector<Action::Type>*>::iterator iterator = postFlopActionHistory.begin(); iterator != postFlopActionHistory.end(); iterator++) {
        numberActionsInHand += postFlopActionHistory[iterator->first]->size();
    }

    double rewardPerAction = reward / numberActionsInHand;
    for(map<string, vector<Action::Type>*>::iterator iterator = preFlopActionHistory.begin(); iterator != preFlopActionHistory.end(); iterator++) {
        vector<Action::Type>* decisionVector = preFlopActionHistory[iterator->first];
        for(unsigned int i = 0; i < decisionVector->size(); i++) {
            preFlopActionMap[iterator->first]->updateEvaluation(decisionVector->at(i), rewardPerAction);
        }
        delete decisionVector;
    }
    preFlopActionHistory.clear();

    for(map<string, vector<Action::Type>*>::iterator iterator = postFlopActionHistory.begin(); iterator != postFlopActionHistory.end(); iterator++) {
        vector<Action::Type>* decisionVector = postFlopActionHistory[iterator->first];
        for(unsigned int i = 0; i < decisionVector->size(); i++) {
            postFlopActionMap[iterator->first]->updateEvaluation(decisionVector->at(i), rewardPerAction);
        }
        delete decisionVector;
    }
    postFlopActionHistory.clear();
}

void ReinforcementLearningPokerAI::writeToFile(string preFlopFilename, string postFlopFilename) {
    ofstream aiPreFlopFile;

    aiPreFlopFile.open (preFlopFilename.data());
    for(map<string, ActionEvaluation*>::iterator iterator = preFlopActionMap.begin(); iterator != preFlopActionMap.end(); iterator++) {
        aiPreFlopFile << iterator->first << "=FOLD:" << preFlopActionMap[iterator->first]->getActionValue(Action::FOLD) << "\n";
        aiPreFlopFile << iterator->first << "=CALL:" << preFlopActionMap[iterator->first]->getActionValue(Action::CALL) << "\n";
        aiPreFlopFile << iterator->first << "=BET:" << preFlopActionMap[iterator->first]->getActionValue(Action::BET) << "\n";
    }
    aiPreFlopFile.close();

    ofstream aiPostFlopFile;
    aiPostFlopFile.open (postFlopFilename.data());
    for(map<string, ActionEvaluation*>::iterator iterator = postFlopActionMap.begin(); iterator != postFlopActionMap.end(); iterator++) {
        aiPostFlopFile << iterator->first << "=FOLD:" << postFlopActionMap[iterator->first]->getActionValue(Action::FOLD) << "\n";
        aiPostFlopFile << iterator->first << "=CALL:" << postFlopActionMap[iterator->first]->getActionValue(Action::CALL) << "\n";
        aiPostFlopFile << iterator->first << "=BET:" << postFlopActionMap[iterator->first]->getActionValue(Action::BET) << "\n";
    }
    aiPostFlopFile.close();
}

void ReinforcementLearningPokerAI::loadFromFile(string preFlopFilename, string postFlopFilename) {
    string line;
    ifstream aiPreFlopFile (preFlopFilename.data());
    if (aiPreFlopFile.is_open())
    {
        while ( getline (aiPreFlopFile,line) )
        {
            int keyAndValueSeparatorIndex = line.find("=");
            string situationKey = line.substr(0, keyAndValueSeparatorIndex);
            if(preFlopActionMap.find(situationKey) == preFlopActionMap.end())
            {
                ActionEvaluation* actionEvaluation = new ActionEvaluation(learningRate, discountFactor, maximumExpectedActionValue);
                preFlopActionMap[situationKey] = actionEvaluation;
            }
            updateActionEvaluationFromFileLine(preFlopActionMap[situationKey], line);
        }
        aiPreFlopFile.close();
    }

    ifstream aiPostFlopFile (postFlopFilename.data());
    if (aiPostFlopFile.is_open())
    {
        while ( getline (aiPostFlopFile,line) )
        {
            int keyAndValueSeparatorIndex = line.find("=");
            string situationKey = line.substr(0, keyAndValueSeparatorIndex);
            if(postFlopActionMap.find(situationKey) == postFlopActionMap.end())
            {
                ActionEvaluation* actionEvaluation = new ActionEvaluation(learningRate, discountFactor, maximumExpectedActionValue);
                postFlopActionMap[situationKey] = actionEvaluation;
            }
            updateActionEvaluationFromFileLine(postFlopActionMap[situationKey], line);
        }
        aiPostFlopFile.close();
    }
}

void ReinforcementLearningPokerAI::updateActionEvaluationFromFileLine(ActionEvaluation* actionEvaluation, string line) {
    int keyAndValueSeparatorIndex = line.find("=");
    string valuePart = line.substr(keyAndValueSeparatorIndex + 1);

    int valueTypeSeparator = valuePart.find(":");
    string type = valuePart.substr(0, valueTypeSeparator);
    string valueAsString = valuePart.substr(valueTypeSeparator + 1);
    double value = atof(valueAsString.data());

    if(type.compare("FOLD") == 0) {
        actionEvaluation->setEvaluation(Action::FOLD, value);
    } else if(type.compare("CALL") == 0) {
        actionEvaluation->setEvaluation(Action::CALL, value);
    } else if(type.compare("BET") == 0) {
        actionEvaluation->setEvaluation(Action::BET, value);
    } else if(type.length() > 0) {
        throw 1;
    }
}
