#include "Hand.h"

#include "Pot.h"
#include <algorithm>

Hand::Hand(int smallBlindLocation, double smallBlindSize, HandEvaluator* handEvaluator)
{
    this->handEvaluator = handEvaluator;
    this->smallBlindLocation = smallBlindLocation;
    this->smallBlindSize = smallBlindSize;
    numberActionsSinceStateStart = 0;
    currentState = NOT_STARTED;
    nextPlayerToAct = -1;
    largestBet = 0;
    dynamicPlayersAtStartOfState = 0;
    lastActionCausedStateChange = false;
    commonCardsNeeded = 0;
    numberOfBetsInState = 0;
    numberOfPlayersAtFlop = 0;
}

Hand::~Hand()
{
    for(unsigned int i = 0; i < actions.size(); i++) {
        Action* temp = actions.back();
        actions.pop_back();
        delete temp;
    }
}

bool Hand::inHand(Player* player) {
    for(unsigned int i = 0; i < players.size(); i++) {
        if(players.at(i) == player) {
            return true;
        }
    }
    return false;
}

bool Hand::playerMayJoin(Player* player) {
    return player->getMoney() >= smallBlindSize * 2;
}

void Hand::addPlayer(Player* player) {
    //can only add players before the hand has started
    if(currentState != NOT_STARTED) {
        throw 1;
    }
    if(!playerMayJoin(player)) {
        throw 2;
    }
    players.push_back(player);
}

void Hand::addAction(Player* player, Action* action) {
    //reset state change flag
    lastActionCausedStateChange = false;

    if(action->getType() == Action::CONTINUE_SIGNAL) {
        nextState();
        return;
    }

    //player can only perform actions if
    if(currentState > NOT_STARTED && currentState < OVER && player == getNextPlayer()) {
        if(action->getType() == Action::FOLD) {
            player->fold();
            unsigned int playersFolded = 0;
            for(unsigned int i = 0; i < players.size(); i++) {
                if(players.at(i)->hasFolded()) {
                    playersFolded++;
                }
            }
            if(playersFolded == players.size() - 1) {
                onHandOver();
                return;
            }
        } else if (action->getType() == Action::CALL) {
            player->addInvestedInHand(largestBet - player->getInvestedInHand());
        } else if (action->getType() == Action::BET) {
            //match current bet (do a call first)
            player->addInvestedInHand(largestBet - player->getInvestedInHand());
            //if player became all in when matching current bet change the action to a call instead of a bet
            if(player->isAllIn()) {
                double amount = action->getAmount();
                delete action;
                action = new Action(Action::CALL, amount);
            } else {
                //else perform the bet
                player->addInvestedInHand(action->getAmount());
                largestBet = player->getInvestedInHand();
                numberOfBetsInState++;
            }
        }
        actions.push_back(action);
        numberActionsSinceStateStart++;
    } else {
        throw 1;
    }

    if(allHadChanceToAct()) {
            //should go to next state if we cannot find a unfolded player who has not matched the top bet
        bool shouldGoToNextState = true;
        for(unsigned int i = 0; i < players.size(); i++) {
            if(!players.at(i)->hasFolded() && players.at(i)->getInvestedInHand() != largestBet && !players.at(i)->isAllIn()) {
                shouldGoToNextState = false;
                break;
            }
        }
        if(shouldGoToNextState) {
            nextState();
        }
    }

    decideNextPlayer();
}

void Hand::onHandOver() {
    currentState = OVER;
    lastActionCausedStateChange = true;
    //possible winners are players that have not folded
    vector<Player*> possibleWinners;
    for(unsigned int i = 0; i < players.size(); i++) {
        if(!players.at(i)->hasFolded()) {
            possibleWinners.push_back(players.at(i));
        } else if (players.at(i)->isAIControlled()) {
            players.at(i)->getAI()->learnFromLastHand(-players.at(i)->getInvestedInHand());
        }
    }
    if(possibleWinners.size() == 0 || possibleWinners.size() > players.size()) {
        throw 6;
    } else if(possibleWinners.size() == 1) {
        possibleWinners.at(0)->addMoney(getPotSize());
        if(possibleWinners.at(0)->isAIControlled()) {
            possibleWinners.at(0)->getAI()->learnFromLastHand(getPotSize() - players.at(0)->getInvestedInHand());
        }
    } else {
        //find all unique bet amounts and sort them to allow construction of sub pots
        vector<double> uniqueBettedAmounts;
        for(unsigned int i = 0; i < possibleWinners.size(); i++) {
            if(std::find(uniqueBettedAmounts.begin(), uniqueBettedAmounts.end(), possibleWinners.at(i)->getInvestedInHand()) == uniqueBettedAmounts.end()) {
                uniqueBettedAmounts.push_back(possibleWinners.at(i)->getInvestedInHand());
            }
        }
        std::sort(uniqueBettedAmounts.begin(), uniqueBettedAmounts.end());

        double amountGivenToSubPotFromEachPlayer = 0;
        for(unsigned int i = 0; i < uniqueBettedAmounts.size(); i++) {
            Pot pot(handEvaluator);
            for(unsigned int j = 0; j < players.size(); j++) {
                if(players.at(j)->getInvestedInHand() >= uniqueBettedAmounts.at(i)) {
                    pot.addPlayer(players.at(j));
                    pot.addChips(uniqueBettedAmounts.at(i) - amountGivenToSubPotFromEachPlayer);
                } else if(players.at(j)->getInvestedInHand() > amountGivenToSubPotFromEachPlayer) {
                    pot.addChips(players.at(j)->getInvestedInHand() - amountGivenToSubPotFromEachPlayer);
                }
            }

            amountGivenToSubPotFromEachPlayer += uniqueBettedAmounts.at(i) - amountGivenToSubPotFromEachPlayer;
            pot.decidePot(commonCards);
        }
    }
}

bool Hand::maximumNumberOfBetsReached() {
    return numberOfBetsInState >= 4;
}

bool Hand::isOver() {
    return currentState == OVER;
}

bool Hand::isStarted() {
    return currentState > NOT_STARTED;
}

void Hand::startHand() {
    if(currentState != NOT_STARTED || players.size() < 2) {
        throw 1;
    } else {
        currentState = PREFLOP;
        dynamicPlayersAtStartOfState = players.size();
        smallBlindLocation = smallBlindLocation % players.size();
        nextPlayerToAct = smallBlindLocation + 2;
        nextPlayerToAct = nextPlayerToAct % players.size();
        players.at(smallBlindLocation)->addInvestedInHand(smallBlindSize);
        players.at((smallBlindLocation + 1) % players.size())->addInvestedInHand(smallBlindSize*2);
        largestBet = smallBlindSize*2;
    }
}

bool Hand::allHadChanceToAct() {
    return numberActionsSinceStateStart >= dynamicPlayersAtStartOfState;
}

bool Hand::hasEnoughPlayersToStart() {
    return players.size() > 1;
}

bool Hand::isPreFlop() {
    return currentState < FLOP;
}

double Hand::getBetSize() {
    if(currentState <= FLOP) {
        return smallBlindSize * 2;
    } else {
        return smallBlindSize * 4;
    }
}

void Hand::decideNextPlayer() {
    if(lastActionCausedStateChange) {
        nextPlayerToAct = smallBlindLocation;
    } else {
        nextPlayerToAct++;
    }

    for(unsigned int i = nextPlayerToAct; i < players.size() + nextPlayerToAct; i++) {
        if(!players.at(i % players.size())->hasFolded() && !players.at(i % players.size())->isAllIn()) {
            nextPlayerToAct = i % players.size();
            break;
        }
    }
}

int Hand::getNumberOfPreflopBets() {
    int preflopBets = 0;
    for(unsigned int i = 0; i < actions.size(); i++) {
        if(actions.at(i)->getType() == Action::BET) {
            preflopBets++;
        } else if(actions.at(i)->getType() == Action::STATE_CHANGE) {
            return preflopBets;
        }
    }
    return preflopBets;
}

int Hand::getNumberOfPostflopBets() {
    int postflopBets = 0;
    for(unsigned int i = 0; i < actions.size(); i++) {
        if(actions.at(i)->getType() == Action::BET) {
            postflopBets++;
        }
    }
    postflopBets -= getNumberOfPreflopBets();
    return postflopBets;
}

int Hand::getNumberOfPlayersAtFlop() {
    return numberOfPlayersAtFlop;
}

int Hand::getLivePlayersRemaining() {
    int playerRemaining = 0;
    for(unsigned int i = 0; i < players.size(); i++) {
        if(!players.at(i)->hasFolded()) {
            playerRemaining++;
        }
    }
    return playerRemaining;
}

bool Hand::anyPlayerMayAct() {
    return dynamicPlayersAtStartOfState > 1 && isStarted() && !isOver();
}

Player* Hand::getNextPlayer() {
    if(currentState == NOT_STARTED || currentState == OVER || nextPlayerToAct == -1) {
        throw 1;
    }
    return players.at(nextPlayerToAct);
}

void Hand::nextState() {
    actions.push_back(new Action(Action::STATE_CHANGE, 0));
    currentState = State(currentState+1);
    if(currentState == FLOP) {
        commonCardsNeeded = 3;
        numberOfPlayersAtFlop = 0;
        for(unsigned int i = 0; i < players.size(); i++) {
            if(!players.at(i)->hasFolded()) {
                numberOfPlayersAtFlop++;
            }
        }
    } else if(currentState == TURN || currentState == RIVER) {
        commonCardsNeeded = 1;
    } else if(currentState == OVER) {
        onHandOver();
    }
    numberActionsSinceStateStart = 0;
    numberOfBetsInState = 0;

    dynamicPlayersAtStartOfState = 0;
    for(unsigned int i = 0; i < players.size(); i++) {
        if(!players.at(i)->hasFolded() && !players.at(i)->isAllIn()) {
            dynamicPlayersAtStartOfState++;
        }
    }

    lastActionCausedStateChange = true;
}

const vector<Player*> Hand::getPlayers() {
    return players;
}

vector<Card*> Hand::getCommonCards() {
    return commonCards;
}

void Hand::dealCommonCard(Card* card) {
    commonCardsNeeded--;
    if(commonCardsNeeded < 0) {
        throw 3;
    }
    commonCards.push_back(card);
}

int Hand::numberCommonCardsNeeded() {
    return commonCardsNeeded;
}

bool Hand::didLastActionCauseStateChange() {
    return lastActionCausedStateChange;
}

double Hand::getLargestBet() {
    return largestBet;
}

double Hand::getPotSize() {
    double potSize = 0;
    for(unsigned int i = 0; i < players.size(); i++) {
        potSize += players.at(i)->getInvestedInHand();
    }
    return potSize;
}
