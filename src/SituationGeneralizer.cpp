#include "SituationGeneralizer.h"

#include <sstream>

SituationGeneralizer::SituationGeneralizer()
{
    //ctor
}

SituationGeneralizer::~SituationGeneralizer()
{
    //dtor
}

string SituationGeneralizer::generatePreFlopSituationKey(int numPlayers, int bets, Card* card1, Card* card2) {
    std::ostringstream keyStream;
    if(bets > 2) {
        bets = 2;
    }
    keyStream << numPlayers / 3 << ":"      //dividing by three to get fewer different situations
    << bets << ":"
    << (card1->getRank() == card2->getRank()) << ":"
    << (card1->getRank() + card2->getRank()) / 2 << ":";    //dividing by two to get fewer different situations

    return keyStream.str();
}

string SituationGeneralizer::generatePostFlopSituationKey(int playersWhoSawFlop,
                                                          int playersRemaining,
                                                          int bets,
                                                          int flushCardsOnBoard,
                                                            int flushCardsInOwnHand,
                                                            HandValue commonCardValue,
                                                            HandValue ownHandValue) {
    int singlePairSituations = 0;
    if(commonCardValue.getHandType() > HandValue::HIGH_CARD) {
        singlePairSituations = 0;   //pair or better on board
    } else if (commonCardValue.getHandType() == HandValue::HIGH_CARD && ownHandValue.getHandType() == HandValue::HIGH_CARD) {
        singlePairSituations = 1;   //no pairs
    } else if( ownHandValue.getHandType() == HandValue::PAIR) {
        if(ownHandValue.getKickerValue(0) <= commonCardValue.getKickerValue(2)) {
            singlePairSituations = 2;   //bottom pair or worse
        } else if(ownHandValue.getKickerValue(0) < commonCardValue.getKickerValue(0)) {
            singlePairSituations = 3;   //pair between top and bottom
        } else {
            singlePairSituations = 4;   //top pair or better
        }
    }

    int likelyFlush = 0;
    if(flushCardsOnBoard >= 4) {
        likelyFlush = 1;
    }
    //only count good flush draws (where the player has two flush cards himself/herself)
    int flushDraw = 0;
    if(flushCardsInOwnHand == 4 && flushCardsOnBoard == 2) {
        flushDraw = 1;
    }
    //set some maximum values to avoid to many different situations
    int betterThanBoard = ownHandValue.getHandType() - commonCardValue.getHandType();
    if(betterThanBoard > 3) {
        betterThanBoard = 3;
    }
    if(bets > 2) {
        bets = 2;
    }
    std::ostringstream keyStream;
    keyStream << playersWhoSawFlop / 3 << ":"   //dividing by three to get fewer different situations
    << playersRemaining / 2 << ":"              //dividing by two to get fewer different situations
    << bets << ":"
    << betterThanBoard << ":"
    << singlePairSituations << ":"
    << likelyFlush << ":"
    << flushDraw << ":"
    ;

    return keyStream.str();
}
