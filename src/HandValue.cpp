#include "HandValue.h"

HandValue::HandValue(HandType handType) : handType(handType)
{
    //ctor
}

HandValue::~HandValue()
{
    //dtor
}

bool HandValue::betterThan(HandValue otherValue) {
    if(handType < otherValue.getHandType() || equalTo(otherValue)) {
        return false;
    }
    else if(handType == otherValue.getHandType()) {
        for(unsigned int i = 0; i < kickerValues.size(); i++) {
            if(kickerValues.at(i) > otherValue.getKickerValue(i)) {
                return true;
            } else if(kickerValues.at(i) < otherValue.getKickerValue(i)) {
                return false;
            }
        }
    }
    return true;
}

bool HandValue::equalTo(HandValue otherValue) {
    if(handType != otherValue.getHandType()) {
        return false;
    }
    for(unsigned int i = 0; i < kickerValues.size(); i++) {
        if(kickerValues.at(i) != otherValue.getKickerValue(i)) {
            return false;
        }
    }
    return true;
}

bool HandValue::worseThan(HandValue otherValue) {
    if(handType > otherValue.getHandType() || equalTo(otherValue)) {
        return false;
    }
    else if(handType == otherValue.getHandType()) {
        for(unsigned int i = 0; i < kickerValues.size(); i++) {
            if(kickerValues.at(i) < otherValue.getKickerValue(i)) {
                return true;
            } else if(kickerValues.at(i) > otherValue.getKickerValue(i)) {
                return false;
            }
        }
    }

    return true;
}

void HandValue::addKickerValue(int kickerValue) {
    kickerValues.push_back(kickerValue);
}

 int HandValue::getKickerValue(int kickerIndex) {
    return kickerValues.at(kickerIndex);
 }


HandValue::HandType HandValue::getHandType() {
    return handType;
 }
