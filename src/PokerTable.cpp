#include "PokerTable.h"

#include <sstream>

using std::ostringstream;

PokerTable::PokerTable(bool needNextHandConfirmation, long maximumNumberOfHandsToPlay) : needNextHandConfirmation(needNextHandConfirmation), maximumNumberOfHandsToPlay(maximumNumberOfHandsToPlay)
{
    cardDeck = new CardDeck();
    currentHand = 0;
    smallBlindLocation = 0;
    handEvaluator = new HandEvaluator();
    proceedToNextHand = true;
    hasSavedAI = false;
    handsPlayed = 0;
}

PokerTable::~PokerTable()
{
    delete cardDeck;
    delete handEvaluator;
}

void PokerTable::addPlayer(Player* player) {
    players.push_back(player);
}

int PokerTable::getPlayerCount() {
    return players.size();
}

vector<Player*> PokerTable::getPlayers() {
    return players;
}

Hand* PokerTable::getCurrentHand() {
    return currentHand;
}

bool PokerTable::hasOngoingHand() {
    return currentHand;
}

void PokerTable::update() {
    if(!hasOngoingHand() || ((proceedToNextHand || !needNextHandConfirmation) && currentHand->isOver() && handsPlayed < maximumNumberOfHandsToPlay)) {
        proceedToNextHand = false;
        dealNewHand();
        handsPlayed++;
    }

    if(currentHand && !currentHand->isStarted() && currentHand->hasEnoughPlayersToStart()) {
        currentHand->startHand();
    }

    if(!currentHand->anyPlayerMayAct() && !currentHand->isOver() && currentHand->isStarted()) {
        currentHand->addAction(0, new Action(Action::CONTINUE_SIGNAL, 0));
        if(currentHand->didLastActionCauseStateChange()) {
            handleHandStateChanged();
        }
    }

    if(currentHand && currentHand->isStarted() && currentHand->anyPlayerMayAct() && currentHand->getNextPlayer()->isAIControlled()) {
        vector<Action*> actions;
        Action* aiAction;
        bool canCheck = currentHand->getNextPlayer()->getInvestedInHand() == currentHand->getLargestBet();
        bool canBet = !currentHand->maximumNumberOfBetsReached();
        if(currentHand->isPreFlop()) {
            string situation = situationGeneralizer.generatePreFlopSituationKey(currentHand->getPlayers().size(),
                                     currentHand->getNumberOfPreflopBets(),
                                     currentHand->getNextPlayer()->getCard1(),
                                     currentHand->getNextPlayer()->getCard2());

            aiAction = currentHand->getNextPlayer()->getAI()->decideNextPreFlopAction(situation, currentHand->getBetSize(), canCheck, canBet);
        } else {
            vector<Card*> partialHand = currentHand->getCommonCards();
            int flushCardsOnBoard = handEvaluator->flushCardsInHand(partialHand);
            HandValue commonCardValue = handEvaluator->getHandValue(partialHand);
            partialHand.push_back(currentHand->getNextPlayer()->getCard1());
            partialHand.push_back(currentHand->getNextPlayer()->getCard2());
            HandValue playerHandValue = handEvaluator->getHandValue(partialHand);
            int flushCardsInHand = handEvaluator->flushCardsInHand(partialHand);

            string situation = situationGeneralizer.generatePostFlopSituationKey(currentHand->getNumberOfPlayersAtFlop(),
                                         currentHand->getLivePlayersRemaining(),
                                         currentHand->getNumberOfPostflopBets(),
                                        flushCardsOnBoard,
                                        flushCardsInHand,
                                         commonCardValue, playerHandValue);

            aiAction = currentHand->getNextPlayer()->getAI()->decideNextPostFlopAction(situation, currentHand->getBetSize(), canCheck, canBet);
        }

        currentHand->addAction(currentHand->getNextPlayer(), aiAction);
        if(currentHand->didLastActionCauseStateChange()) {
            handleHandStateChanged();
        }
    }

    if(!hasSavedAI && currentHand && ((!currentHand->isStarted() && !currentHand->hasEnoughPlayersToStart()) || handsPlayed >= maximumNumberOfHandsToPlay)) {
        hasSavedAI = true;
        for(unsigned int i = 0; i < players.size(); i++) {
            if(players.at(i)->isAIControlled()) {
                ostringstream preFlopFilenameStream;
                preFlopFilenameStream << "preFlopAI" << i << ".txt";
                ostringstream postFlopFilenameStream;
                postFlopFilenameStream << "postFlopAI" << i << ".txt";
                players.at(i)->getAI()->writeToFile(preFlopFilenameStream.str(), postFlopFilenameStream.str());
            }
        }
    }
}


void PokerTable::allowToProceedToNextHand() {
    proceedToNextHand = true;
}

void PokerTable::dealNewHand() {
    //return last hands cards to the deck
    if(hasOngoingHand()) {
        unsigned int playersInHand = currentHand->getPlayers().size();
        for(unsigned int i = 0; i < playersInHand; i++) {
            cardDeck->insertCard(currentHand->getPlayers().at(i)->getCard1());
            cardDeck->insertCard(currentHand->getPlayers().at(i)->getCard2());
        }
        for(unsigned int i = 0; i < currentHand->getCommonCards().size(); i++) {
            cardDeck->insertCard(currentHand->getCommonCards().at(i));
        }
        delete currentHand;
    }
    cardDeck->shuffle();
    if(cardDeck->cardsInDeck() != 52) {
        throw 1;
    }
    //deal from the deck to all the players
    smallBlindLocation = (smallBlindLocation + 1) % players.size();
    currentHand = new Hand(smallBlindLocation, SMALL_BLIND_SIZE, handEvaluator);
    for(unsigned int i = 0; i < players.size(); i++) {
        if(currentHand->playerMayJoin((players.at(i)))) {
            Card* card1 = cardDeck->getTopCard();
            Card* card2 = cardDeck->getTopCard();
            players.at(i)->giveNewHand(card1, card2);
            currentHand->addPlayer(players.at(i));
        }
    }
}

void PokerTable::currentPlayerFolded() {
    if(currentPlayerAllowedToAct()) {
        currentHand->addAction(currentHand->getNextPlayer(), new Action(Action::FOLD, 0));
        if(currentHand->didLastActionCauseStateChange()) {
            handleHandStateChanged();
        }
    }
}

void PokerTable::currentPlayerCalled() {
    if(currentPlayerAllowedToAct()) {
        currentHand->addAction(currentHand->getNextPlayer(), new Action(Action::CALL, 0));
        if(currentHand->didLastActionCauseStateChange()) {
            handleHandStateChanged();
        }
    }
}

void PokerTable::currentPlayerBetted() {
    if(currentPlayerAllowedToAct()) {
        currentHand->addAction(currentHand->getNextPlayer(), new Action(Action::BET, currentHand->getBetSize()));
        if(currentHand->didLastActionCauseStateChange()) {
            handleHandStateChanged();
        }
    }
}

void PokerTable::handleHandStateChanged() {
    while(currentHand->numberCommonCardsNeeded() > 0) {
        Card* card = cardDeck->getTopCard();
        currentHand->dealCommonCard(card);
    }
}

bool PokerTable::currentPlayerAllowedToAct() {
    return hasOngoingHand() && currentHand->isStarted() && !currentHand->isOver();
}

int PokerTable::getSmallBlindLocation() {
    return smallBlindLocation;
}
