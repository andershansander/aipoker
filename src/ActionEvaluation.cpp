#include "ActionEvaluation.h"

#include <cstdlib>

ActionEvaluation::ActionEvaluation(double learningRate, double discountFactor, double probableMaxValue) : learningRate(learningRate), discountFactor(discountFactor), probableMaxValue(probableMaxValue)
{
    actionValueMap[Action::BET] = 0;
    actionValueMap[Action::CALL] = 0;
    actionValueMap[Action::FOLD] = 0;
}

ActionEvaluation::~ActionEvaluation()
{
    //dtor
}

double ActionEvaluation::getActionValue(Action::Type type) {
    if(type == Action::BET || type == Action::CALL || type == Action::FOLD) {
        return actionValueMap[type];
    } else {
        return 0;
    }
}

void ActionEvaluation::updateEvaluation(Action::Type type, double reward) {
    actionValueMap[type] = actionValueMap[type] + learningRate * (reward + discountFactor * probableMaxValue - actionValueMap[type]);
}

void ActionEvaluation::setEvaluation(Action::Type type, double reward) {
    actionValueMap[type] = reward;
}


Action::Type ActionEvaluation::getNextActionToTry(bool canCheck, bool canBet) {
    double lowest = actionValueMap[Action::FOLD];
    Action::Type lowestType = Action::FOLD;

    double secondLowest;
    Action::Type secondLowestType;

    double highest;
    Action::Type highestType;

    if(actionValueMap[Action::CALL] < lowest) {
        secondLowest = lowest;
        secondLowestType = lowestType;
        lowest = actionValueMap[Action::CALL];
        lowestType = Action::CALL;
    } else {
        secondLowest = actionValueMap[Action::CALL];
        secondLowestType = Action::CALL;
    }

    if(actionValueMap[Action::BET] < lowest) {
        highest = secondLowest;
        highestType = secondLowestType;
        secondLowest = lowest;
        secondLowestType = lowestType;
        lowest = actionValueMap[Action::BET];
        lowestType = Action::BET;
    } else if(actionValueMap[Action::BET] < secondLowest) {
        highest = secondLowest;
        highestType = secondLowestType;
        secondLowest = actionValueMap[Action::BET];
        secondLowestType = Action::BET;
    } else {
        highest = actionValueMap[Action::BET];
        highestType = Action::BET;
    }

    if(lowest < 0) {
        double avoidNegativeNumbers = 0 - lowest;
        lowest += avoidNegativeNumbers;
        secondLowest += avoidNegativeNumbers;
        highest += avoidNegativeNumbers;
    }

    double sum = highest + lowest + secondLowest;

    //if lowest value is 0 add five percent of the sum to allow the worst option to be tried in rare circumstances
    if(lowest == 0) {
        double addFivePercent = sum * 0.05;
        lowest += addFivePercent;
        secondLowest += addFivePercent;
        highest += addFivePercent;
        sum = highest + lowest + secondLowest;
    }

    //if all values are 0 add some values to give all options equal chances
    if(sum == 0) {
        lowest += 10;
        secondLowest += 10;
        highest += 10;
        sum = highest + lowest + secondLowest;
    }

    double randomValue = (double)rand() / RAND_MAX;
    randomValue = randomValue * sum;

    Action::Type result;
    if(randomValue < lowest) {
        result = lowestType;
    }
    else if(randomValue > lowest && randomValue < lowest + secondLowest) {
        result = secondLowestType;
    }
    else {
        result = highestType;
    }

    if(result == Action::FOLD && canCheck) {
        return Action::CALL;
    } else if(result == Action::BET && !canBet) {
        return Action::CALL;
    } else {
        return result;
    }
}
