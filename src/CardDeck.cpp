#include "CardDeck.h"

#include <vector>
#include <ctime>
#include <cstdlib>

using std::vector;

CardDeck::CardDeck()
{
    for(int suit = 0; suit < 4; suit++) {
        for(int i = 2; i <= 14; i++) {
            Card* card = new Card(i, Card::Suit(suit));
            insertCard(card);
        }
    }
}

CardDeck::~CardDeck()
{
    //dtor
    while(cards.size() > 0) {
        Card* card = getTopCard();
        delete card;
    }
}

void CardDeck::shuffle() {
    srand(time(0));
    for(int i = 0; i < 1000; i++) {
        int randCard1 = rand() % cards.size();
        int randCard2 = rand() % cards.size();
        std::iter_swap(cards.begin() + randCard1, cards.begin() + randCard2);
    }
}

Card* CardDeck::getTopCard() {
    Card* temp = cards.back();
    cards.pop_back();
    return temp;
}

void CardDeck::insertCard(Card* card) {
    //throw exception if we are inserting an existing card
    for(unsigned int i = 0; i < cards.size(); i++) {
        if(cards.at(i)->getRank() == card->getRank() && cards.at(i)->getSuit() == card->getSuit()) {
            throw 4;
        }
    }
    cards.push_back(card);
}

int CardDeck::cardsInDeck() {
    return cards.size();
}
