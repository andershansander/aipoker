#include "Card.h"

Card::Card(int rank, Suit suit) : rank(rank), suit(suit) {

}

Card::~Card()
{
    //dtor
}

int Card::getRank() {
    return rank;
}

Card::Suit Card::getSuit() {
    return suit;
}
